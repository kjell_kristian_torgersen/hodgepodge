#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mosquitto.h>
#include "mqtt.h"

void mosq_log_callback(struct mosquitto * mosq, void *userdata, int level, const char *str)
{
	/* Pring all log messages regardless of level. */

	switch (level) {
	//case MOSQ_LOG_DEBUG:
	//case MOSQ_LOG_INFO:
	//case MOSQ_LOG_NOTICE:
	case MOSQ_LOG_WARNING:
	case MOSQ_LOG_ERR: {
		printf("%i:%s\n", level, str);
	}
	}
}

void mqt_on_message_callback(struct mosquitto * mosc, void * param, const struct mosquitto_message * msg) {
	printf("Received: %s %s", msg->topic, (char*)msg->payload);
}

int check(int status)
{
	if (status != MOSQ_ERR_SUCCESS) {
		fprintf(stderr, "Error: %s\n", mosquitto_strerror(status));
	}
	return status;
}

/** \brief Connect to broker with user name and password
 * \param *host Host name or IP-address as zero terminated text string
 * \param port Port number. Typically 1883
 * \param *username Username or NULL for no user name
 * \param *password Password or NULL for no password
 * \return A handle for this connection to the MQTT broker. */
mqtt_handle_t mqtt_connect_auth(const char * host, int port, const char * username, const char *password)
{
	struct mosquitto * mosq;
	int keepalive = 60;
	bool clean_session = true;
	int status;
	mosquitto_lib_init();
	mosq = mosquitto_new(NULL, clean_session, NULL);
	if (!mosq) {
		fprintf(stderr, "Error: Out of memory. \n");
		return NULL;
	}

	if (username || password) {
		status = mosquitto_username_pw_set(mosq, username, password);
		if (check(status))
			return NULL;
	}

	mosquitto_log_callback_set(mosq, mosq_log_callback);
	mosquitto_message_callback_set(mosq, mqt_on_message_callback);

	status = mosquitto_connect(mosq, host, port, keepalive);
	if (check(status)) {
		fprintf(stderr, "Error: Unable to connect.\n");
		return NULL;
	}

	mosquitto_subscribe(mosq, NULL, "test/topic", 0);

	status = mosquitto_loop_start(mosq);
	if (check(status)) {
		fprintf(stderr, "Error: Unable to start loop.\n");
		return NULL;
	}
	return mosq;
}

/** \brief Connect to a broker without authentication.
 * \param *host Host name or IP address as zero terminated text string.
 * \param port Port number. Typically 1883.
 * \return A handle for this connection to the MQTT broker. */
mqtt_handle_t mqtt_connect(const char * host, int port)
{
	return mqtt_connect_auth(host, port, NULL, NULL);
}

/** \brief Send a zero terminated text string to broker with specified topic.
 * \param mqtt Handle for connection to broker.
 * \param *topic Topic as zero terminated text string
 * \param *msg Message to send as zero terminated text string.
 * \return 0 on success, and non-zero on failure. */
int mqtt_send(mqtt_handle_t mqtt, const char * topic, const char *msg)
{
	int status = mosquitto_publish(mqtt, NULL, topic, strlen(msg), msg, 0, 0);
	check(status);
	return status;
}

/** \brief Send n byte long message to broker with specified topic
 * \param mqtt Handle for connection to broker.
 * \param *topic Topic as zero terminated text string
 * \param *buf Message to send.
 * \param n Length of message
 * \return 0 on success, and non-zero on failure. */
int mqtt_write(mqtt_handle_t mqtt, const char * topic, const char *buf, int n)
{
	return mosquitto_publish(mqtt, NULL, topic, n, buf, 0, 0);
}

/** \brief Disconnect from broker.
 * \param mqtt Handle for connection with broker. */
void mqtt_disconnect(mqtt_handle_t mqtt)
{
	mosquitto_disconnect(mqtt);
	mosquitto_loop_stop(mqtt, false);
	mosquitto_destroy(mqtt);
}
