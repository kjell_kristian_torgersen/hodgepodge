#ifndef MQTT_H_
#define MQTT_H_

/// An opaque struct pointer
typedef struct mosquitto * mqtt_handle_t;

mqtt_handle_t mqtt_connect(const char * host, int port);
mqtt_handle_t mqtt_connect_auth(const char * host, int port, const char * username, const char *password);
int mqtt_send(mqtt_handle_t mqtt, const char * topic, const char *msg);
int mqtt_write(mqtt_handle_t mqtt, const char * topic, const char *buf, int n);
void mqtt_disconnect(mqtt_handle_t mqtt);

#endif /* MQTT_H_ */
